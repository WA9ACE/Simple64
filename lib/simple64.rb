require 'base64'

class Simple64
  
  def encode file
    File.open(file, 'r') do |file|
      return @encoded_file = Base64.encode64(file.read)
    end
  end
end