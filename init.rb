$LOAD_PATH << File.join(Dir.getwd, 'lib')

require 'sinatra'
require 'simple64'

before do
  @base = "do something"
end

get '/' do
  erb :index, :layout => :layout
end

post '/encode' do
  begin
    @file = params[:inputfile][:tempfile]
    @name = params[:inputfile][:filename]
    @size = params[:inputfile][:size]
    @simple64 = Simple64.new
    @encoded_file = @simple64.encode(@file)
    erb :encoded
  rescue NoMethodError => e
    erb :file_error
  end
end

get '/archives' do
  erb :archives
end

get '/random' do
  erb :random
end

get '/about' do
  erb :about
end

not_found do  
  status 404  
  erb :error  
end